// ==UserScript==
// @name           twitter2nitter
// @namespace      renametwitter
// @description    change urls from twitter to nitter
// @include        *
// ==/UserScript==

var debug = false;

function log(msg) {
  if (debug) {
    unsafeWindow.console.log(msg);
  }
}

function t2n_replace(eventname) {
  log("running from "+ eventname);
  
  window.setTimeout(
    function(){
      var newLinks = document.querySelectorAll('a[href*="https://twitter.com"],a[href*="http://twitter.com"]');

      if (newLinks.length > 0) {
        for (var i = 0; i < newLinks.length; i++) {
          log("OLD URL");
          log(newLinks[i].href);

          newLinks[i].href = newLinks[i].href.replace('twitter.com','nitter.net');

          log("NEW URL");
          log(newLinks[i].href);
        }
      }
    },
    200);
}


var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    t2n_replace(mutation.type);
  });   
});
  
var observerConfig = {
  attributes: true,
  subtree: true,
  childList: true,
  characterData: true
};
  
// Listen to all changes to body and child nodes
observer.observe(document.documentElement, observerConfig);

// Run once DOM ready
t2n_replace('first');

log("twittet2nitter was run");

/*  // DOMNodeInserted depricated according to MDN
document.addEventListener('DOMNodeInserted',function(e){
	t2n_replace("DOMNodeInserted");
	},
false);
*/

